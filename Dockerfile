FROM golang:1.11 AS build
WORKDIR /go/src/app
COPY . .

# RUN go get -d -v ./...
# /go/src/gitlab.com/hbouvier
RUN mkdir -p /go/src/gitlab.com/hbouvier/couchdb /go/src/gitlab.com/http && \
    cd /go/src/gitlab.com/hbouvier && \
    git clone --single-branch --branch WIP https://gitlab.com/hbouvier/couchdb && \
    git clone https://gitlab.com/hbouvier/http && \
    cd /go/src/app

RUN go install -v ./...
RUN ls
RUN ls /go/bin
FROM ubuntu

COPY --from=build /go/bin/app /app/operator

CMD /app/operator
