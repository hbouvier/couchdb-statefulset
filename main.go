package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"gitlab.com/hbouvier/couchdb"
)

var version = "snapshot"

func main() {
	log.Println("Versions: Sample=v" + version + ", CouchDB-API=v" + couchdb.Version())

	log.Println("Installing signal handlers")
	signalsChanel := make(chan os.Signal, 1)
	signal.Notify(signalsChanel, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP, syscall.SIGQUIT)
	timeToTerminate := make(chan struct{})
	var canTerminateSafely sync.WaitGroup // canTerminateSafely.Add(1) in each go routine

	log.Println("Clustering CouchDB")
	go work(&canTerminateSafely, timeToTerminate, signalsChanel)

	signals := map[os.Signal]int{
		syscall.SIGHUP:  1,
		syscall.SIGINT:  2,
		syscall.SIGQUIT: 3,
		syscall.SIGTERM: 15,
	}
	sig := <-signalsChanel
	var code = 0
	if sig != nil {
		code = 128 + signals[sig]
		log.Printf("Shutdown signal (" + strconv.Itoa(signals[sig]) + ") received, exiting...")
		close(timeToTerminate)
	}
	log.Println("Waiting for all threads to finish")
	canTerminateSafely.Wait()
	log.Println("Terminating with exit code " + strconv.Itoa(code))
	os.Exit(code)
}

func work(canTerminateSafely *sync.WaitGroup, timeToTerminate chan struct{}, signalsChanel chan os.Signal) {
	canTerminateSafely.Add(1)
	defer func() {
		canTerminateSafely.Done()
		close(signalsChanel)
	}()

	fqdn := couchdb.FullyQualifiedDomainName()
	log.Println(fmt.Sprintf("%v", fqdn))
	list, err := fqdn.LookupDNSServiceRecored()
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(fmt.Sprintf("%v", list))

	creds := couchdb.Authentication{"admin", "admin"}
	server := couchdb.Connect("127.0.0.1", 5984, 5986, creds)
	for {
		err := server.Ready()
		if err == nil {
			break
		}
		log.Println(err)
		select {
		case <-time.After(time.Second): // wait one second
			continue
		case <-timeToTerminate: // got
			return
		}
	}

	state, err := server.GetClusterState()
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("CouchDB cluster state: " + state.State)

	members, err := server.GetClusterMembership()
	if err != nil {
		log.Println(err)
		return
	}
	members.AllNodes.Each(func(name string) {
		log.Println("  Known node => " + name)
	})
	members.ClusterNodes.Each(func(name string) {
		log.Println("  Joined node => " + name)
	})

	if state.State != "cluster_finished" {
		status, err := server.ChangeClusterState(couchdb.Actions.FinishClusterAction)
		if err != nil {
			log.Println(err)
			return
		}
		log.Println("CouchDB Cluster Configured [" + strconv.FormatBool(status) + "]")
	}

	// name := "nonode@nohost"
	// name := "couchdb@couchdb-0"
	name := "couchdb@couchdb-1.couchdb-discovery.default.svc.cluster.local"
	addedNode, err := server.AddNode(name)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("CouchDB add node " + name + " [" + strconv.FormatBool(addedNode.Ok) + "]")
}

/*
	nodesNotInCluster := removeValues(
		discoveredServers.Map(func(longname string) string {
			return db.NodeName(clustername, longname)
		}),
		membership.(db.MembershipResponse).ClusterNodes)

		var ok interface{}
	var lastError error
	for _, nodeName := range nodesNotInCluster {
		log.Info("nodeNotInCluster", nodeName)
		ok, err = couchdbLocal.Execute(couchdbLocal.Retry(db.AddNode(nodeName), thirtySeconds, db.OneSecond, nil))
		if err != nil {
			lastError = fmt.Errorf("Unable to add node %s => %+v", nodeName, err)
		} else if ok.(bool) != true {
			lastError = fmt.Errorf("Unable to add node %s => ok: false", nodeName)
		}
	}
	return lastError

*/
