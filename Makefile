VERSION=$(shell cat VERSION)
GO=go
GO_FLAGS=-ldflags "-X main.version=${VERSION}"

all:	run

install:
	${GO} install ${GO_FLAGS}

run:
	${GO} run main.go